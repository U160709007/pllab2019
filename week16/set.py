import time

#reading the file and for set data structure
with open("text.txt") as f:   
    s = set([line.rstrip('\n') for line in f])
start = time.time()
    
end = time.time()
print(end-start)
#write into the output file.
Outfile=open("output.txt","a+")
Outfile.write(str(s))
Outfile.close()
